﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEngine.UI;

public class TaskManager : MonoBehaviour
{

    public int GroundTruth;
    private MenuManager MenuManager;
    private TrajectoryManager TrajectoryManager;
    private TimerManager TimerManager;

    public bool taskIsReady;
    public bool taskIsStart;    // true: user start
    public bool taskIsSetup;
    public bool taskIsPause;
    public int QuestionID = 0;
    public int StepID = 0;

    public GameObject T1, T2, T3, T4, O1, O2, O3, O4;
    public Text T1RN, T2RN, T3RN, T4RN;
    public TPlayerController TC1, TC2, TC3, TC4;
    public OPlayerController OC1, OC2, OC3, OC4;

    public GameObject M1, M2, M3, M4;
    public SkinnedMeshRenderer meshRenderer;
    public Material TMN1, TMN2, TMN3, TMN4, TMS;


    private List<List<string>> MDNQuestion = new List<List<string>>();
    private List<List<string>> MENQuestion = new List<List<string>>();
    private List<List<string>> MDCQuestion = new List<List<string>>();
    private List<List<string>> MECQuestion = new List<List<string>>();
    public List<string> ChangeTime = new List<string>();
    public List<string> ChangePlayer = new List<string>();

    void Awake()
    {

        MenuManager = GameObject.FindObjectOfType<MenuManager>();
        TrajectoryManager = GameObject.FindObjectOfType<TrajectoryManager>();
        TimerManager = GameObject.FindObjectOfType<TimerManager>();

    }

    // Start is called before the first frame update
    
    void Start()
    {

        TextAsset MDNs = Resources.Load<TextAsset>("Question/MovableDisableNumberAccumulation");
        Split_Question(MDNs.text, "MDN");
        TextAsset MENs = Resources.Load<TextAsset>("Question/MovableEnableNumberAccumulation");
        Split_Question(MENs.text, "MEN");
        TextAsset MDCs = Resources.Load<TextAsset>("Question/MovableDisableColorChange");
        Split_Question(MDCs.text, "MDC");
        TextAsset MECs = Resources.Load<TextAsset>("Question/MovableEnableColorChange");
        Split_Question(MECs.text, "MEC");
        TextAsset CTs = Resources.Load<TextAsset>("Question/ChangeTime");
        Split_ChangeTimePlayer(CTs.text, "CT");
        TextAsset CPs = Resources.Load<TextAsset>("Question/ChangePlayer");
        Split_ChangeTimePlayer(CPs.text, "CP");


        TC1 = T1.GetComponent<TPlayerController>();
        TC2 = T2.GetComponent<TPlayerController>();
        TC3 = T3.GetComponent<TPlayerController>();
        TC4 = T4.GetComponent<TPlayerController>();

        OC1 = O1.GetComponent<OPlayerController>();
        OC2 = O2.GetComponent<OPlayerController>();
        OC3 = O3.GetComponent<OPlayerController>();
        OC4 = O4.GetComponent<OPlayerController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(taskIsStart)
        {
            if(MenuManager.ConfigureType == "MovableDisableNumberAccumulation")
            {
                T1RN.text = MDNQuestion[QuestionID][0];
                T2RN.text = MDNQuestion[QuestionID][1];
                T3RN.text = MDNQuestion[QuestionID][2];
                T4RN.text = MDNQuestion[QuestionID][3];
            }
            else if(MenuManager.ConfigureType == "MovableEnableNumberAccumulation")
            {
                T1RN.text = MENQuestion[QuestionID][0];
                T2RN.text = MENQuestion[QuestionID][1];
                T3RN.text = MENQuestion[QuestionID][2];
                T4RN.text = MENQuestion[QuestionID][3];
            }
            else if(MenuManager.ConfigureType == "MovableDisableColorChange")
            {
                T1RN.text = MDCQuestion[QuestionID][0];
                T2RN.text = MDCQuestion[QuestionID][1];
                T3RN.text = MDCQuestion[QuestionID][2];
                T4RN.text = MDCQuestion[QuestionID][3];
            }
            else
            {
                T1RN.text = MECQuestion[QuestionID][0];
                T2RN.text = MECQuestion[QuestionID][1];
                T3RN.text = MECQuestion[QuestionID][2];
                T4RN.text = MECQuestion[QuestionID][3];
            }

        }
    }

    public void Init_Task()
    {
        taskIsStart = false;
        taskIsSetup = false;
        taskIsPause = false;
        QuestionID = 0;
        StepID = 0;

        T1RN.text = "";
        T2RN.text = "";
        T3RN.text = "";
        T4RN.text = "";

        meshRenderer = M1.GetComponent<SkinnedMeshRenderer>();
        meshRenderer.material = TMN1;
        meshRenderer = M2.GetComponent<SkinnedMeshRenderer>();
        meshRenderer.material = TMN2;
        meshRenderer = M3.GetComponent<SkinnedMeshRenderer>();
        meshRenderer.material = TMN3;
        meshRenderer = M4.GetComponent<SkinnedMeshRenderer>();
        meshRenderer.material = TMN4;
        TC1.Init_Player();
        TC2.Init_Player();
        TC3.Init_Player();
        TC4.Init_Player();
        OC1.Init_Player();
        OC2.Init_Player();
        OC3.Init_Player();
        OC4.Init_Player();

    }

    public void Init_Pos()
    {
        T1.transform.position = new Vector3(-3.5f, 0.0f, 6.5f);
        T2.transform.position = new Vector3(3.5f, 0.0f, 6.5f);
        T3.transform.position = new Vector3(-2.2f, 0.0f, 11f);
        T4.transform.position = new Vector3(2.2f, 0.0f, 11f);

        O1.transform.position = new Vector3(-3.0f, 0.0f, 5.5f);
        O2.transform.position = new Vector3(3.0f, 0.0f, 5.5f);
        O3.transform.position = new Vector3(-1.7f, 0.0f, 10f);
        O4.transform.position = new Vector3(1.7f, 0.0f, 10f);

        T1.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
        T2.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
        T3.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
        T4.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);

        O1.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
        O2.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
        O3.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
        O4.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);

    }

    public void Split_Question(string questions, string type)
    {
        string[] content = questions.Split('\n');
        for(int i = 0 ; i < content.Length ; i++)
        {
            List<string> questionList = content[i].Split(' ').ToList();
            if(type == "MDN")
                MDNQuestion.Add(questionList);
            else if(type == "MEN")
                MENQuestion.Add(questionList);
            else if(type == "MDC")
                MDCQuestion.Add(questionList);
            else
                MECQuestion.Add(questionList);

        }
    }

    public void Split_ChangeTimePlayer(string questions, string type)
    {
        string[] content = questions.Split('\n');
        for(int i = 0 ; i < content.Length ; i++)
        {
            if(type == "CT")
                ChangeTime.Add(content[i]);
            if(type == "CP")
                ChangePlayer.Add(content[i]);
        }
    }

    public void Init_Question()
    {
        StepID = 0;

        int playerID = int.Parse(ChangePlayer[QuestionID]);

        if(MenuManager.ConfigureType == "MovableDisableNumberAccumulation")
            GroundTruth = int.Parse(MDNQuestion[QuestionID][0]) + int.Parse(MDNQuestion[QuestionID][1]) + int.Parse(MDNQuestion[QuestionID][2]) + int.Parse(MDNQuestion[QuestionID][3]);
        else if(MenuManager.ConfigureType == "MovableEnableNumberAccumulation")
            GroundTruth = int.Parse(MENQuestion[QuestionID][0]) + int.Parse(MENQuestion[QuestionID][1]) + int.Parse(MENQuestion[QuestionID][2]) + int.Parse(MENQuestion[QuestionID][3]);
        else if(MenuManager.ConfigureType == "MovableDisableColorChange")
            GroundTruth = int.Parse(MDCQuestion[QuestionID][playerID - 1]);
        else
            GroundTruth = int.Parse(MECQuestion[QuestionID][playerID - 1]);
    }

    public void Player_NextPosition()
    {

        if(!taskIsPause)
        {
            if (StepID < TrajectoryManager.TrajectorySteps[QuestionID])
            {
                TC1.MoveOn_NextPos(QuestionID, StepID);
                TC2.MoveOn_NextPos(QuestionID, StepID);
                TC3.MoveOn_NextPos(QuestionID, StepID);
                TC4.MoveOn_NextPos(QuestionID, StepID);

                // 如果有對手
                if(MenuManager.OpponentType == "Enable")
                {
                    OC1.MoveOn_NextPos(QuestionID, StepID);
                    OC2.MoveOn_NextPos(QuestionID, StepID);
                    OC3.MoveOn_NextPos(QuestionID, StepID);
                    OC4.MoveOn_NextPos(QuestionID, StepID);
                }

                StepID = StepID + 1;
            }
        }
    }

    public void Player_ChangeColor()
    {
        int playerID = int.Parse(ChangePlayer[QuestionID]);
        if(playerID == 1)
            meshRenderer = M1.GetComponent<SkinnedMeshRenderer>();
        else if(playerID == 2)
            meshRenderer = M2.GetComponent<SkinnedMeshRenderer>();
        else if(playerID == 3)
            meshRenderer = M3.GetComponent<SkinnedMeshRenderer>();
        else
            meshRenderer = M4.GetComponent<SkinnedMeshRenderer>();
        
        meshRenderer.material = TMS;
    }

    public void Player_ChangeColorBack()
    {
        int playerID = int.Parse(ChangePlayer[QuestionID]);
        if(playerID == 1)
            meshRenderer.material = TMN1;
        else if(playerID == 2)
            meshRenderer.material = TMN2;
        else if(playerID == 3)
            meshRenderer.material = TMN3;
        else
            meshRenderer.material = TMN4;

    }

    public void Setup_TaskContent()
    {
        TimerManager.Initial_StartTimer();
        TimerManager.Initial_QuestionTimer(); 
    }

    public void ReadyTask()
    {

        if(MenuManager.OpponentType == "Disable")
        {
            O1.transform.position = new Vector3(100f, 0.0f, 5.5f);
            O2.transform.position = new Vector3(100f, 0.0f, 5.5f);
            O3.transform.position = new Vector3(100f, 0.0f, 10f);
            O4.transform.position = new Vector3(100f, 0.0f, 10f);
        }

        TimerManager.Initial_StartTimer();
        taskIsReady = true;
    }
}
