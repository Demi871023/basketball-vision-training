﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtUser : MonoBehaviour
{
    // Start is called before the first frame update
    
    public GameObject user;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.LookAt(user.transform.position);
        gameObject.transform.eulerAngles = gameObject.transform.eulerAngles + new Vector3(0.0f, 180.0f, 0.0f);
    }
}
